package com.smartstreet;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.Profile;

import java.io.InputStream;

/**
 * Activity that displays profile picture and name.
 */
public class ProfileActivity extends FragmentActivity {
    public static Intent createIntent(Context context) {
        return new Intent(context, ProfileActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        setTitle("Profile Details");
        final Profile profile = Profile.getCurrentProfile();
        // start async-task to download picture
        new DownloadImageTask((ImageView) findViewById(R.id.profile_image))
                .execute(profile.getProfilePictureUri(300, 500).toString());
        ((TextView) findViewById(R.id.profile_name)).setText(profile.getFirstName() + " " +
                profile.getLastName());
    }

    /**
     * Task to background download an image.
     */
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView mImageView;

        public DownloadImageTask(ImageView imageView) {
            this.mImageView = imageView;
        }

        protected Bitmap doInBackground(String... urls) {
            String urlToDownloadImage = urls[0];
            Bitmap icon = null;
            try {
                InputStream in = new java.net.URL(urlToDownloadImage).openStream();
                icon = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return icon;
        }

        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                // set the downloaded image in the imageview.
                mImageView.setImageBitmap(result);
            }
        }
    }
}
