package com.smartstreet;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginManager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends FragmentActivity implements View.OnClickListener{
    public static Intent createIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        final Profile profile = Profile.getCurrentProfile();
        if (profile != null) {
            menu.findItem(R.id.menu_profile_details).setTitle(profile.getFirstName() + " "
                    + profile.getLastName());
        } else {
            menu.removeItem(R.id.menu_profile_details);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_profile_details: {
                startActivity(ProfileActivity.createIntent(this));
                return true;
            }
            case R.id.menu_log_out: {
                LoginManager.getInstance().logOut();
                startActivity(LoginActivity.createIntent(this));
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());

        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.smartstreet", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Set Barcode button listener
        findViewById(R.id.button_bar_code).setOnClickListener(this);
        // Set Camera button listener
        findViewById(R.id.button_camera).setOnClickListener(this);
        // Set Maps button listener
        findViewById(R.id.button_maps).setOnClickListener(this);
        // Set Comments button listener
        findViewById(R.id.button_comments).setOnClickListener(this);
        // Set Microphone button listener
        findViewById(R.id.button_microphone).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        final int id = v.getId();
        switch (id) {
            case R.id.button_bar_code : {
                startActivity(BarcodeReaderActivity.createIntent(this));
                break;
            }
            case R.id.button_camera : {
                startActivity(CameraFeaturesActivity.createIntent(this));
                break;
            }
            case R.id.button_maps : {
                startActivity(MapsActivity.createIntent(this));
                break;
            }
            case R.id.button_comments : {
                startActivity(CommentsActivity.createIntent(this));
                break;
            }
            case R.id.button_microphone : {
                startActivity(MicrophoneActivity.createIntent(this));
                break;
            }
            default: {
                break;
            }
        }
    }
}
