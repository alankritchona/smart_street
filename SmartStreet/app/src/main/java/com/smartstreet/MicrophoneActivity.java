package com.smartstreet;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.IOException;

/**
 * Activity that handles the microphone recording.
 */
public class MicrophoneActivity extends FragmentActivity implements View.OnClickListener {
    private static String mFileName = Environment.getExternalStorageDirectory().getAbsolutePath()
            + "/audiorecordtest.3gp";

    private Button mRecordButton;
    private MediaRecorder mRecorder;
    boolean mStartRecording = true;

    private Button mPlayButton;
    private MediaPlayer mPlayer;
    boolean mStartPlaying = true;

    public static Intent createIntent(Context context) {
        final Intent intent = new Intent(context, MicrophoneActivity.class);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_microphone);
        mRecordButton = (Button) findViewById(R.id.button_record);
        mRecordButton.setOnClickListener(this);
        mPlayButton = (Button) findViewById(R.id.button_play);
        mPlayButton.setOnClickListener(this);
        findViewById(R.id.button_share).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_record) {
            handleRecording();
        } else if (v.getId() == R.id.button_play) {
            handlePlaying();
        } else if (v.getId() == R.id.button_share) {
            try {
                // Creates the Share intent
                final Uri audioUri = Uri.fromFile(new File(mFileName));
                final Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Intent.EXTRA_STREAM, audioUri);
                intent.setType("audio/*");
                startActivity(intent);
            } catch (Exception e) {
            }
        }
    }

    private void handleRecording() {
        if (mStartRecording) {
            // Start the recording if the recording flag is true
            mRecorder = new MediaRecorder();
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setOutputFile(mFileName);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            try {
                mRecorder.prepare();
            } catch (IOException e) {
            }
            mRecorder.start();
        } else {
            // Stop the recording if the recording flag is false
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
        }
        // Update recording button text
        mRecordButton.setText(mStartRecording ? "Stop recording" : "Start recording");
        mStartRecording = !mStartRecording;
    }

    private void handlePlaying() {
        if (mStartPlaying) {
            // Start playing if the playing flag is true;
            mPlayer = new MediaPlayer();
            try {
                mPlayer.setDataSource(mFileName);
                mPlayer.prepare();
                mPlayer.start();
            } catch (IOException e) {
            }
        } else {
            // Stop playing if the playing flag is false;
            mPlayer.release();
            mPlayer = null;
        }
        // Update playing button text
        mPlayButton.setText(mStartPlaying ? "Stop playing" : "Start playing");
        mStartPlaying = !mStartPlaying;
    }

    @Override
    public void onPause() {
        super.onPause();
        // Release recorder when activity is paused.
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }

        // Release player when activity is paused.
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }
}
