package com.smartstreet;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.VideoView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Activity that handles the clicking and displaying of images/videos.
 */
public class CameraFeaturesActivity extends FragmentActivity implements View.OnClickListener,
        AdapterView.OnItemClickListener {
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_VIDEO_CAPTURE = 2;

    private PicturesAdapter picturesAdapter;
    private ListView pictureListView;

    private VideosAdapter videosAdapter;
    private ListView videosListView;

    public static Intent createIntent(Context context) {
        final Intent intent = new Intent(context, CameraFeaturesActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        findViewById(R.id.button_take_picture).setOnClickListener(this);
        findViewById(R.id.button_take_video).setOnClickListener(this);

        pictureListView = (ListView) findViewById(R.id.images_listview);
        // Initialize the picturesAdapter from the already taken pictures.
        picturesAdapter = createLatestPicturesAdapter();
        pictureListView.setAdapter(picturesAdapter);
        pictureListView.setOnItemClickListener(this);

        // Initialize the videosAdapter from the already taken videos.
        videosListView = (ListView) findViewById(R.id.videos_listview);
        videosAdapter = createLatestVideosAdapter();
        videosListView.setAdapter(videosAdapter);
        videosListView.setOnItemClickListener(this);
    }

    private PicturesAdapter createLatestPicturesAdapter() {
        Bitmap[] smartTreePictureArray =
                new Bitmap[SmartCircuitTree.getInstance().getPictures().size()];
        // Creates an picturesAdapter from the array of pictures stored in SmartCircuitTree's current
        // instance.
        return new PicturesAdapter(this, R.layout.row_picture_adapter,
                SmartCircuitTree.getInstance().getPictures().toArray(smartTreePictureArray));
    }

    private VideosAdapter createLatestVideosAdapter() {
        Uri[] smartTreeVideosArray = new Uri[SmartCircuitTree.getInstance().getVideos().size()];
        // Creates an vidoesAdapter from the array of videos stored in SmartCircuitTree's current
        // instance.
        return new VideosAdapter(this, R.layout.row_videos_adapter,
                SmartCircuitTree.getInstance().getVideos().toArray(smartTreeVideosArray));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_take_picture) {
            try {
                dispatchTakePictureIntent();
            } catch (IOException e) {
            }
        } else if (v.getId() == R.id.button_take_video) {
            dispatchTakeVideoIntent();
        }
    }

    private void dispatchTakePictureIntent() throws IOException {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            if (extras != null && extras.get("data") != null) {
                // Add the latest clicked image to the singleton and recreate the listview picturesAdapter.
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                SmartCircuitTree.getInstance().insertPicture(imageBitmap);
                picturesAdapter = createLatestPicturesAdapter();
                pictureListView.setAdapter(picturesAdapter);
            }
        } else if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
            Uri videoUri = data.getData();
            if (videoUri != null) {
                // Add the latest video to the singleton and recreate the listview vidoesAdapter.
                SmartCircuitTree.getInstance().insertVideo(videoUri);
                videosAdapter = createLatestVideosAdapter();
                videosListView.setAdapter(videosAdapter);
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (view.findViewById(R.id.picture) != null) {
            // Opens the share with multiple apps feature on clicking any image.
            final ImageView imageView = (ImageView) view.findViewById(R.id.picture);
            final Bitmap imageBitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
            if (imageBitmap != null) {
                try {
                    // Create a temporary file to allow sharing of the stored bitmap.
                    File file = new File(getExternalCacheDir(), new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + ".png");
                    file.createNewFile();

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    imageBitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                    byte[] bitmapdata = bos.toByteArray();

                    //write the bytes in file
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();

                    // Creates the Share intent
                    final Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
                    intent.setType("image/png");
                    startActivity(intent);
                } catch (Exception e) {
                }
            }
        } else {
            final VideoView videoView = (VideoView) view.findViewById(R.id.video);
            MediaController mc = new MediaController(this);
            videoView.setMediaController(mc);
            videoView.start();
        }

    }

    /**
     * The picturesAdapter that provides the images for the images listview.
     */
    private class PicturesAdapter extends ArrayAdapter<Bitmap> {

        private Bitmap[] items;

        public PicturesAdapter(Context context, int textViewResourceId,
                               Bitmap[] items) {
            super(context, textViewResourceId, items);
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.row_picture_adapter, null);
            }

            Bitmap it = items[position];
            if (it != null) {
                ImageView iv = (ImageView) v.findViewById(R.id.picture);
                if (iv != null) {
                    iv.setImageBitmap(it);
                }
            }

            return v;
        }
    }

    /**
     * The vidoesAdapter that provides the videos for the videos listview.
     */
    private class VideosAdapter extends ArrayAdapter<Uri> {
        private Uri[] items;

        public VideosAdapter(Context context, int resource, Uri[] items) {
            super(context, resource, items);
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.row_videos_adapter, null);
            }

            final Uri uri = items[position];
            if (uri != null) {
                final VideoView videoView = (VideoView) v.findViewById(R.id.video);
                if (videoView != null) {
                    videoView.setVideoURI(uri);

                    final ImageView button = (ImageView) v.findViewById(R.id.button_share);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                // Creates the Share intent
                                final Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra(Intent.EXTRA_STREAM, uri);
                                intent.setType("video/*");
                                startActivity(intent);
                            } catch (Exception e) {

                            }
                        }
                    });
                }
            }
            return v;
        }
    }
}
